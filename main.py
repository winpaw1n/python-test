from flask import Flask,request
from database import writeDB, readDB

app = Flask(__name__)

@app.route('/')
def index():
    return 'Index Page'

@app.route('/hello')
def hello():
    return 'Hello, World'

@app.route('/write-row', methods=['POST', 'GET'])
def writeRow():
    error = None
    if request.method == 'POST':
        print(request.data)
        print(request.json)
        request_data = request.json
        writeDB(request_data)
        return 'ok'

print(readDB())
    
def data():
    pass